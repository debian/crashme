crashme (2.8.5-2) unstable; urgency=medium

  * QA upload.
  * Take over package maintenance via ITS process. (Closes: #1088927)
  * debian/control: Set maintainer to Debian QA Group.
  * debian/control:
    + Bump Standards-Version to 4.7.0.
    + Bump debhelper compat to v13.
    + Update Vcs-* fields.
  * debian/patches/0002-crashme.c-Include-missing-headers-for-syscall.patch:
    Add patch to fix FTBFS due to missing headers. (Closes: #1066596)

 -- Boyuan Yang <byang@debian.org>  Sat, 21 Dec 2024 11:31:48 -0500

crashme (2.8.5-1) unstable; urgency=medium

  * Imported Upstream version 2.8.5
    - Closes: #755759

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Wed, 06 Aug 2014 16:30:58 +0200

crashme (2.8.2-1) unstable; urgency=medium

  * Imported Upstream version 2.8.2
  * No longer broken if heap is non-executable (Closes: #749816)
  * Removed fix-exec.patch, merged in upstream
  * Removed fix-memset patch, integrated by upstream
  * Refreshed fix-spelling patch

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Wed, 02 Jul 2014 08:40:49 +0200

crashme (2.7-1) unstable; urgency=medium

  * Imported Upstream version 2.7
  * Added mt19937ar.* to copyright 
  * New maintainer (Closes: #739083)
  * Added Makefile patch to enable hardening 
  * Spelling patch refreshed (fix spelling in documentation and source code)
  * VCS fields in canonical format
  * Bump standards to 3.9.5 (no changes needed)
  * Created manpage for pddet binary
  * d/rules updated to clean cleanly
  * Deleted d/patches/legacy.patch, the changes were integrated in 2.7
  * Added fix exec patch
  * Added patch fixing memset transposed arguments
  * Updated Upstream-Contact in d/copyright
  * Added d/crashme.insall
  * Removed d/crashme.lintian-overrides
  * d/watch updated to a new URL location
  * Updated README.Debian
  * Updated descriptions in d/control

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Fri, 09 May 2014 10:17:15 +0200

crashme (2.4-11) unstable; urgency=low

  * Found, extracted and installed the upstream changelog

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 08 Jul 2012 18:40:50 +0100

crashme (2.4-10) unstable; urgency=low

  * New maintainer (Closes: #677765)
  * Added VCS fields to debian/control
  * Moved to source format '3.0 (quilt)'
  * Updated standards version to 3.9.3
  * Updated debhelper version to 9 and compat level to 9
  * Modernized debian/rules and debian/copyright
  * Annotated legacy.patch and added spelling.patch
  * Added lintian overrides for lack of upstream changelog
    and lack of hardening during the build
  * Added doc-base support

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 08 Jul 2012 14:44:56 +0100

crashme (2.4-9) unstable; urgency=low

  * Adopt the package (Closes: #353388).
  * Conform to policy with CFLAGS set to "-O2 -g -Wall".
  * Fix resulting gcc warnings from turning on "-Wall".

 -- Aurélien GÉRÔME <ag@roxor.cx>  Mon, 31 Jul 2006 14:41:43 +0200

crashme (2.4-8) unstable; urgency=low

  * QA upload.
  * debian/postinst: Remove; /usr/doc already handled by the old prerm.
  * debian/rules:
    - Add support for DEB_BUILD_OPTIONS=noopt.
    - Use dh_install.

 -- Matej Vela <vela@debian.org>  Sun, 30 Jul 2006 22:14:56 +0200

crashme (2.4-7) unstable; urgency=low

  * QA upload.
  * s/PAQUETE/crashme/g in postinst. Sorry 

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Wed, 12 Jul 2006 21:44:04 +0200

crashme (2.4-6) unstable; urgency=low

  * QA upload.
  * This package is orphaned as of #353388, so setting the Maintainer field
    accordingly to QA.
  * Fix typo in package description (Closes: #363215).
  * Get rid of the /usr/doc link (Closes: #359371).
  * Stop echoing warnings and stuff on console in postinst. Debconf should be
    used instead. Somebody please fix this.
  * Bumped Standards version to 3.7.2. No changes needed.
  * Fixed manual section 

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Wed, 12 Jul 2006 18:47:31 +0200

crashme (2.4-5) unstable; urgency=low

  * New maintainer (Closes: #81889)
  * Bump up Standards-Version
  * Add Build-Depends (Closes: #70344)
  * Remove pddet from the package since it has no manpage and does not seem to
    be useful, even for the build process.

 -- Aaron Lehmann <aaronl@vitelus.com>  Wed, 10 Jan 2001 19:29:36 -0800

crashme (2.4-4) unstable; urgency=low

  * Replaced execl call with execlp. (Fixes bug #37304)
  * Removed access to an obsolete structure member. (Fixes bug #37446)

 -- Jay Kominek <jay.kominek@colorado.edu>  Tue, 25 May 1999 09:32:17 -0600

crashme (2.4-3) unstable; urgency=low

  * Switched binary-arch and binary-indep to what they should be.
    (Fixes bug #25452)

 -- Jay Kominek <jkominek@debian.org>  Thu,  6 Aug 1998 03:34:49 -0600

crashme (2.4-2) unstable; urgency=low

  * Changed extended package description to format sanely in dselect.
    (Fixes bug #23666)

 -- Jay Kominek <jkominek@debian.org>  Thu, 18 Jun 1998 02:57:30 -0400

crashme (2.4-1) unstable; urgency=low

  * Initial Release.

 -- Jay Kominek <jkominek@debian.org>  Sat,  6 Jun 1998 19:12:44 -0400
